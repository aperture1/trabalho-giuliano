using System.ComponentModel.DataAnnotations.Schema;

namespace fantasyWorld.Models
{
    public class userModel
    {
        public enum RoleEnum { 
            comprador,
            fornecedor 
        };

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string email { get; set; }
        public string senha { get; set; }        
        public string Role { get; set; }

        public string uf { get; set; }
        public string nome { get; set; }
        public string cep { get; set; }
        public string cidade { get; set; }
        public string endereco { get; set; }
        public string documento { get; set; }
        public string telefone { get; set; }
    }
}
