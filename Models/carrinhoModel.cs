using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace fantasyWorld.Models
{
    public class carrinhoModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long idCarrinho { get; set; }
        public int qtd { get; set; }
        public int valor { get; set; }
        public int subtotal { get; }
    }
}
