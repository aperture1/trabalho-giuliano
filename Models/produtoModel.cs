using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace fantasyWorld.Models
{
    public class produtoModel
    {
        public enum categoryList
        {
            Roupa,
            Peruca,
            Lentes,
            Acessórios,    
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public string titulo { get; set; }
        public string marca { get; set; }
        public string tamanho { get; set; }
        public string descricao { get; set; }
        public categoryList Class { get; set; }
    }
}
