using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using fantasyWorld.Data;
using fantasyWorld.Helpers;
using fantasyWorld.Models;

namespace fantasyWorld.Services
{
    public interface ILoginService
    {
        (userModel userModel, string token) Authenticate(Login login);
    }

    public sealed class LoginService : ILoginService
    {
        private readonly ApiContext _dbContext;
        private readonly SecuritySettings _securitySettings;

        public LoginService(ApiContext dbContext, IOptions<SecuritySettings> securitySettings)
        {
            this._dbContext = dbContext;
            this._securitySettings = securitySettings.Value;
        }

        public (userModel userModel, string token) Authenticate(Login login)
        {
            var userModel = _dbContext.userModel.SingleOrDefault(u => 
            u.email == login.email && u.senha == 
            AuthenticationHelper.ComputeHash(login.senha));
            
            // return null if userModel not found
            if (userModel == null)
                return (null, null);

            // discard senha 
            userModel.senha = string.Empty;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_securitySettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, userModel.Id.ToString()),
                    new Claim(ClaimTypes.Role, userModel.Role.ToString())
                }),
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            
            return (userModel, tokenHandler.WriteToken(token));
        }
    }
}
