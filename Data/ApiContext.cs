using Microsoft.EntityFrameworkCore;
using fantasyWorld.Models;
using fantasyWorld.Helpers;

namespace fantasyWorld.Data
{
  public class ApiContext : DbContext
  {
    public ApiContext(DbContextOptions<ApiContext> options)
      : base(options)
    {}
    
    public DbSet<userModel> userModel { get; set; }
    public DbSet<produtoModel> produtoModel { get; set; }
    public DbSet<carrinhoModel> carrinhoModel { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      // initial user. password must be changed later.
      modelBuilder.Entity<userModel>()
        .HasData(new userModel { Id = 1, email = "comprador@email.com", senha = AuthenticationHelper.ComputeHash("123"), 
        Role = fantasyWorld.Models.userModel.RoleEnum.comprador.ToString() });

      // set uUsername as Unique
      modelBuilder.Entity<userModel>()
        .HasIndex(u => u.email)
        .IsUnique();
    }
  }
}
