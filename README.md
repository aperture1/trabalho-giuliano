<h1> Projeto Aperture </h1>

<p>Fantasy World é uma loja física de vendas de cosplay de todos os tipos e tamanhos que possui um público fiel há alguns anos, entretanto, surgiu-se a necessidade de expandir os negócios e aumentar de forma eficiente o público atingido pela loja. Com esse objetivo, foi proposto ao time Aperture a criação de um site e-commerce que possua um ambiente agradável e intuitivo de forma a aumentar as vendas. </p>  

<p>As ferramentas utilizadas para a criação do mesmo serão: C# DOT.NET. A metodologia utilizada para a elaboração do projeto será o SCRUM.</p>

<h1>Time Aperture </h1>

<p><b>Nome: </b>Amanda Eques Pereira <b>     Papel: </b> Project Owner <b>   RA:</b> 1911515695</p>
<p><b>Nome: </b>Larissa Gonçalves Gouvea <b> Papel: </b> Dev. Full Stack <b> RA: </b> 1911514011</p>
<p><b>Nome: </b>Nicole Mendes Padovani <b>   Papel: </b> Scrum Master <b>    RA: </b> 1911513568</p>