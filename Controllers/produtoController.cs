using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using fantasyWorld.Data;
using fantasyWorld.Models;
using fantasyWorld.Services;

namespace fantasyWorld.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class produtoController : ControllerBase
    {
        private readonly ApiContext _context;

        public produtoController(ApiContext context)
        {
            _context = context;
        }

        // GET: api/produto
        [HttpGet]
        public async Task<ActionResult<IEnumerable<produtoModel>>> listarProduto()
        {
            return await _context.produtoModel.ToListAsync();
        }

        // GET: api/produto/5
        [HttpGet("{id}")]
        public async Task<ActionResult<produtoModel>> buscarProduto(long id)
        {
            var produtoModel = await _context.produtoModel.FindAsync(id);

            if (produtoModel == null)
            {
                return NotFound();
            }

            return produtoModel;
        }

        // PUT: api/produto/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> editarProduto(long id, produtoModel produtoModel)
        {
            if (id != produtoModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(produtoModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!produtoModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/produto
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<produtoModel>> cadastroProduto(produtoModel produtoModel)
        {
            _context.produtoModel.Add(produtoModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetprodutoModel", new { id = produtoModel.Id }, produtoModel);
        }

        // DELETE: api/produto/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<produtoModel>> removerProduto(long id)
        {
            var produtoModel = await _context.produtoModel.FindAsync(id);
            if (produtoModel == null)
            {
                return NotFound();
            }

            _context.produtoModel.Remove(produtoModel);
            await _context.SaveChangesAsync();

            return produtoModel;
        }

        private bool produtoModelExists(long id)
        {
            return _context.produtoModel.Any(e => e.Id == id);
        }
    }
}
