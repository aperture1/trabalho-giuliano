using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using fantasyWorld.Data;
using fantasyWorld.Models;
using fantasyWorld.Services;

namespace fantasyWorld.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "comprador")]
    public class UserController : ControllerBase
    {
        private readonly ApiContext _context;

        public UserController(ApiContext context)
        {
            _context = context;
        }

        // GET: api/User
        [HttpGet]
        public async Task<ActionResult<IEnumerable<dynamic>>> getUser()
        {
            // return await _context.userModel.ToListAsync();
            return await _context.userModel.Select(u => MapUser(u)).ToListAsync();
        }

        // GET: api/User/5
        [HttpGet("{id}")]
        public async Task<ActionResult<userModel>> getUserByID(int id)
        {
            var userModel = await _context.userModel.FindAsync(id);

            if (userModel == null)
            {
                return NotFound();
            }
            return MapUser(userModel);
        }

        // PUT: api/User/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> editarUsuario(int id, userModel userModel)
        {
            if (id != userModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(userModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!userModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/User
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<userModel>> editarUsuario(userModel userModel)
        {
            _context.userModel.Add(userModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetuserModel", new { id = userModel.Id }, MapUser(userModel));
        }

        private static dynamic MapUser(userModel user) 
        {
            return new
            {
                Id = user.Id,
                email = user.email   
            };
        }

        private bool userModelExists(int id)
        {
            return _context.userModel.Any(e => e.Id == id);
        }
    }
}
